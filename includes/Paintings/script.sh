for f in *
do
    if [ -d "$f" ]
    then
        for ff in $f/*
        do      
            newname=`echo $ff | sed -e 's/ /_/g'`
  			mv "$ff" "$newname"
            echo "Processing $ff"
        done
    else
    	newname=`echo $f | sed -e 's/ /_/g'`
  		mv "$f" "$newname"
        echo "Processing file $f"
    fi
done
