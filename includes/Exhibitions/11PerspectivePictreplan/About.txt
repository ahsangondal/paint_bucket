Artist Statement:

"Travelling has been a very important part of my life.
 It inspires what I create and how I create .
I doodle as a way of taking things out of my head and making sense of what is happening around me. 
Absorbing the experiences and then making them permanent on paper.
 There is no message which needs to be deciphered. It is just an imagery diary of my feelings and obsessions."