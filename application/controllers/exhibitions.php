<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class exhibitions extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->vars('base_url', base_url);
		$this->load->vars('includes_dir', includes_dir);
		$this->load->vars('current_url', $this->uri->uri_to_assoc(1));
		$this->load->model ( 'paint_model' );
		$this->data['exhibitions']=$this->paint_model->getExhibitions();

		$this->load->view('exhibitions',$this->data);
	}

	public function details()
	{

		$this->load->vars('base_url', base_url);
		$this->load->vars('includes_dir', includes_dir);
		$this->load->vars('current_url', $this->uri->uri_to_assoc(1));

		$this->load->model ( 'paint_model' );


		$this->data['artwork']=$this->paint_model->getExArtWork($this->uri->segment(3));
		$this->data['exhibitionInfo']=$this->paint_model->getExhibitionInfo($this->uri->segment(3));
		$this->load->view('exhibitionDetail',$this->data);

	}

	public function artwork()
	{

		$this->load->vars('base_url', base_url);
		$this->load->vars('includes_dir', includes_dir);
		$this->load->vars('current_url', $this->uri->uri_to_assoc(1));

		$this->load->model ( 'paint_model' );


		$this->data['artwork']=$this->paint_model->getExPainting($this->uri->segment(3));
		// $this->data['artistBio']=$this->paint_model->getArtistBio($this->uri->segment(3));
		$this->load->view('exArtwork',$this->data);

	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */