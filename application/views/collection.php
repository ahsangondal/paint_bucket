<!DOCTYPE html>
<html lang="en">
	<head>
	<!-- Meta -->
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
		<meta name = "description" content = "Our world renown collection." />
		
	<!-- Favicon -->
		<link rel="shortcut icon" href="images/favicon.ico" />
		
	<!-- Styles -->
		<link href="<?php echo $includes_dir?>styles/main.css" rel="stylesheet" />
		
	<!-- Plugins -->
		<link href="<?php echo $includes_dir?>plugins/flexslider/flexslider.css" rel="stylesheet" />
		<link href="<?php echo $includes_dir?>plugins/flexslider/skin.css" rel="stylesheet" />
		<link href="<?php echo $includes_dir?>plugins/colorbox/colorbox.css" rel="stylesheet" />
		<link href="<?php echo $includes_dir?>plugins/colorbox/skin.css" rel="stylesheet" />
		<link href="<?php echo $includes_dir?>plugins/mediaelement/mediaelementplayer.min.css" rel="stylesheet" />
		<link href="<?php echo $includes_dir?>plugins/mediaelement/skin.css" rel="stylesheet" />
		
	<!-- IE -->
		<!--[if lt IE 9]>
			<script src="scripts/ie/html5.js"></script>
			<link href="styles/ie/ie.css" rel="stylesheet" />
		<![endif]-->
		
	<!-- Title -->
		<title>The PaintBucket Gallery</title>
	</head>
	
	<body>
	<!-- MAIN WRAP -->
		<div id="main-wrap">
				
			<div id="page" class="fullscreen no-scroll">
				<ul id="home-accordion">
					
					
					<li>
						<a href="<?echo $base_url;?>collection/artists/3d">
							<img data-pos="0.52" src="<?php echo $includes_dir?>collectionpage/3d.jpg" alt="Image" />
						</a>
						
					</li>
					
					<li>
						<a href="<? echo $base_url; ?>collection/artists/Digital_art">
							<img data-pos="0.5" src="<?php echo $includes_dir?>collectionpage/digital.jpg" alt="Image" />
						</a>
						
					</li>
					
					<li>
						<a href="<? echo $base_url; ?>collection/artists/Paintings">
							<img data-pos="0.5" src="<?php echo $includes_dir?>collectionpage/painting.jpg" alt="Image" />
						</a>
						
					</li>
					
					
				</ul>
				
			</div><!-- #page -->
			
			<div id="background">
			</div><!-- #background -->
			
			<div id="dock">
				 <header id="header">
					<div id="site-logo">
						<a href="index.html">
							<img src="<?php echo $includes_dir?>img/logo.png" alt="Site Logo" />
						</a>
					</div><!-- #site-logo -->
					
					<nav id="main-nav">
						<ul>
                			<? $this->load->view('menu');?>
							
						</ul>

					</nav><!-- #main-nav -->
				 </header><!-- #header -->
				 
				 <footer id="footer">
					<div id="copyright" >
						<p >&copy; <a href="http://ahsangondal.com" class="normal">Ahsan Gondal </a> All Rights Reserved</p>
						<!-- <p>True Story is a responsive HTML5 template by <a href="#">Tee Fouad</a></p> -->
					</div><!-- #copyright -->
					
					<div id="social-media">

						<p>Stay Connected</p>
						<a href="#" class="icon twitter">Twitter</a>
						<a href="#" class="icon facebook">Facebook</a>
						<a href="#" class="icon linkedin">LinkedIn</a>
					</div><!-- #social-media -->
				 </footer><!-- #footer -->
				 
				 <a href="#" id="toggle-dock"><span><span>
					<span class="active-text">Hide dock</span>
					<span class="inactive-text">Show dock</span>
				</span></span></a>
				
				 <a href="#" id="page-top-link"><span><span>Back to top</span></span></a>
			</div><!-- #dock -->
			
			<div id="preloader">
				<span><span>Loading</span></span>
			</div><!-- #preloader -->
			
		</div><!-- #main-wrap -->
		
	<!-- SCRIPTS -->
		<script src="<?php echo $includes_dir?>scripts/jquery.1.7.2.js"></script>
		<script src="<?php echo $includes_dir?>scripts/jquery.easing.js"></script>
		<script src="<?php echo $includes_dir?>scripts/jquery.cookie.js"></script>
		<script src="<?php echo $includes_dir?>scripts/main.js"></script>
		
	<!-- Plugins -->
		<script src="<?php echo $includes_dir?>plugins/modernizr/modernizr.min.js"></script>
		<script src="<?php echo $includes_dir?>plugins/flexslider/jquery.flexslider.min.js"></script>
		<script src="<?php echo $includes_dir?>plugins/colorbox/jquery.colorbox.min.js"></script>
		<script src="<?php echo $includes_dir?>plugins/mediaelement/mediaelement-and-player.min.js"></script>
		<script src="<?php echo $includes_dir?>plugins/social/jquery.social.js"></script>
		<script type="text/javascript">
			$("#collection").addClass("current");

		</script>
	<!-- IE -->
		<!--[if lt IE 9]>
			<script src="<?php echo $includes_dir?>scripts/ie/ie.js" type="text/javascript"></script>
		<![endif]-->
	</body>
</html>