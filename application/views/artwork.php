<div class="container">
	<div class="row">
        <div id="speaker-detail" class="">
            <div class="row">
            	<button title="Close (Esc)" type="button" class="mfp-close">×</button>
                
                <div class="col-md-8  no-padding">
                    <img class="img-responsive" src="<?echo  $includes_dir;?><?echo $artwork[0]->imagePath;?>" alt="" />
                </div>
                    
                <div class="col-md-4 ">
                    <h2><?echo $artwork[0]->contentTitle;?></h2>
                    <br>
                    
                    <p class="lead"><?echo $artwork[0]->contentDescription;?></p>
                    
                    <br>
                    <p class="lead"><?echo $artwork[0]->contentSize;?></p>
                    <br>
                    
                    <div class="center">

                    <a href="<?echo $base_url?>contact" class="btn btn-dark btn-transparent ">Wanna Know More? </a>
                    </div>
<!--                     <ul class="social list-inline list-unstyled">
                    	<li><a href=""><i class="fa fa-2x fa-facebook-square"></i></a></li>
                        <li><a href=""><i class="fa fa-2x fa-twitter-square"></i></a></li>
                        <li><a href=""><i class="fa fa-2x fa-google-plus-square"></i></a></li>
                        <li><a href=""><i class="fa fa-2x fa-linkedin-square"></i></a></li>
                    </ul>
                     -->
                    <div id="content">
                    </div>
                </div>
            
            </div>
        </div>
    </div>
</div>