<li class="treeview " id="artists">
              <a href="<?echo $base_url?>auth_admin/dashboard">
                <i class="fa fa-dashboard "></i> <span>Manage Artists (Collection)</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
            </li>
<li class="treeview " id="ex">
              <a href="<?echo $base_url?>auth_admin/exhibitions">
                <i class="fa fa-dashboard "></i> <span>Manage Exhibitions</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
            </li>

            <li class="treeview " id="ex">
              <a href="<?echo $base_url?>auth_admin/press">
                <i class="fa fa-dashboard "></i> <span>Manage Press</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
            </li>
<li class="treeview " id="ex">
              <a href="<?echo $base_url?>auth/logout">
                <i class="fa fa-dashboard "></i> <span>Logout</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
            </li>
