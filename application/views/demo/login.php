<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>The Paint Bucket | Log in</title>
<meta
  content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'
  name='viewport'>
<!-- Bootstrap 3.3.2 -->
<link href="<?php echo $includes_dir;?>AdminLTE/bootstrap/css/bootstrap.min.css"
  rel="stylesheet" type="text/css" />
<!-- Font Awesome Icons -->
<link
  href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"
  rel="stylesheet" type="text/css" />
<!-- Theme style -->
<link href="<?php echo $includes_dir;?>AdminLTE/dist/css/AdminLTE.min.css"
  rel="stylesheet" type="text/css" />
<!-- iCheck -->
<link href="<?php echo $includes_dir;?>AdminLTE/plugins/iCheck/square/blue.css"
  rel="stylesheet" type="text/css" />

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body class="login-page">
  <div class="login-box">
    <div class="login-logo">
      <a href="../../index2.html"><b>The Paint </b>Bucket</a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
      
        <div class="content clearfix">
      <div class="col100">
        <!-- <h2>User Login</h2> -->

      <?php if (! empty($message)) { ?>
        <div id="message">
          <?php echo $message; ?>
        </div>
      <?php } ?>
        
        <?php echo form_open(current_url(), 'class="parallel"');?>    
          <fieldset class="w50 parallel_target">
            
                <label for="identity">Email or Username:</label>
                <input class="form-control" type="text" id="identity" name="login_identity" value="<?php echo set_value('login_identity', '');?>" class="tooltip_parent"/>
                
                <label for="password">Password:</label>
                <input class="form-control" type="password" id="password" name="login_password" value="<?php echo set_value('login_password', '');?>"/>
            
            <?php 
              # Below are 2 examples, the first shows how to implement 'reCaptcha' (By Google - http://www.google.com/recaptcha),
              # the second shows 'math_captcha' - a simple math question based captcha that is native to the flexi auth library. 
              # This example is setup to use reCaptcha by default, if using math_captcha, ensure the 'auth' controller and 'demo_auth_model' are updated.
            
              # reCAPTCHA Example
              # To activate reCAPTCHA, ensure the 'if' statement immediately below is uncommented and then comment out the math captcha 'if' statement further below.
              # You will also need to enable the recaptcha examples in 'controllers/auth.php', and 'models/demo_auth_model.php'.
              #/*
              if (isset($captcha)) 
              { 
                echo "<li>\n";
                echo $captcha;
                echo "</li>\n";
              }
              #*/
              
              /* math_captcha Example
              # To activate math_captcha, ensure the 'if' statement immediately below is uncommented and then comment out the reCAPTCHA 'if' statement just above.
              # You will also need to enable the math_captcha examples in 'controllers/auth.php', and 'models/demo_auth_model.php'.
              if (isset($captcha))
              {
                echo "<li>\n";
                echo "<label for=\"captcha\">Captcha Question:</label>\n";
                echo $captcha.' = <input class="form-control" type="text" id="captcha" name="login_captcha" class="width_50"/>'."\n";
                echo "</li>\n";
              }
              #*/
            ?>
              
              
                <label for="submit">Login:</label>
                <input class="form-control btn btn-primary" type="submit" name="login_user" id="submit" value="Submit" class="link_button large"/>
              
              
            
          </fieldset>

          
        <?php echo form_close();?>
      </div>
    </div>


    </div>
  </div>
  <!-- /.login-box -->

  <!-- jQuery 2.1.3 -->
  <script
    src="<?php echo $includes_dir;?>AdminLTE/plugins/jQuery/jQuery-2.1.3.min.js"></script>
  <!-- Bootstrap 3.3.2 JS -->
  <script src="<?php echo $includes_dir;?>AdminLTE/bootstrap/js/bootstrap.min.js"
    type="text/javascript"></script>
  
</body>
</html>