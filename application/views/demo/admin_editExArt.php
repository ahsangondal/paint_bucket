<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AdminLTE 2 | Data Tables</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?echo $includes_dir;?>AdminLTE/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/dt/dt-1.10.9,b-1.0.3,cr-1.2.0,fc-3.1.0,fh-3.0.0,kt-2.0.0,r-1.0.7,rr-1.0.0,sc-1.3.0,se-1.0.1/datatables.min.css"/>
 

    <!-- Theme style -->
    <link rel="stylesheet" href="<?echo $includes_dir;?>AdminLTE/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?echo $includes_dir;?>AdminLTE/dist/css/skins/_all-skins.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="<?echo $includes_dir;?>AdminLTE/index2.html" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>T</b>PB</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>The</b>Paint Bucket</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
              
              <!-- Notifications: style can be found in dropdown.less -->
              
              <!-- Control Sidebar Toggle Button -->
              
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            
          </div>
          <!-- search form -->
          <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
             
            </div>
          </form>
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <? $this->load->view('demo/nav'); ?>
           
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            
          </h1>
          <ol class="breadcrumb">
            
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header with-border">
                  <!-- <h3 class="box-title">Quick Example</h3> -->
                </div><!-- /.box-header -->
                <!-- form start -->
                <? foreach ($artistBio as $AB) { ?>
                
                <?php 
                $attributes = array('id' => 'form1');
                echo form_open_multipart('auth_admin/editExArt_save',$attributes);?>
                  <div class="box-body">
                    <div class="form-group">
                      <!-- <label for="artistSlug">Artist Slug</label> -->
                      <input type="hidden" class="form-control" id="contentId" name="contentId" placeholder="Enter text" value="<?echo $AB->contentId?>" data-validation="required">
                      <input type="hidden" class="form-control" id="Slug" name="Slug" placeholder="Enter text" value="<?echo $AB->Slug?>" data-validation="required">
                      <input type="hidden" class="form-control" id="Path" name="Path" placeholder="Enter text" value="<?echo $AB->Path?>" data-validation="required">
                      <input type="hidden" class="form-control" id="ExhibitionName" name="ExhibitionName" placeholder="Enter text" value="<?echo $AB->ExhibitionName?>" data-validation="required">
                      
                    </div>

                    <div class="form-group">
                      <label for="ArtistName">Artist Name</label>
                      <input type="text" class="form-control" id="ArtistName" name="ArtistName" placeholder="Enter text" value=" <?echo $AB->ArtistName ?>" data-validation="required">
                    </div>
                    <div class="form-group">
                      <label for="Title">Title</label>
                      <input type="text" class="form-control" id="Title" name="Title" placeholder="Enter text" value=" <?echo $AB->Title ?>" data-validation="required">
                    </div>
                    <div class="form-group">
                      <label for="Description">Description</label>

                      <textarea id="Description" name="Description" rows="10" cols="80"><?echo $AB->Description ?></textarea>
                    </div>
                    <div class="form-group">
                      <label for="Size">Size</label>
                      <input type="text" class="form-control" id="Size" name="Size" placeholder="Enter text" value=" <?echo $AB->Size ?>" data-validation="required">
                    </div>
                     <div class="form-group">
                      <label for="artistImage">Upload New Image</label>
                      <input type="file" name="userfile" size="20" >
                      <!-- <p class="help-block">Example block-level help text here.</p> -->
                    </div>

                    <div class="col-xs-6">
                      <label for="imagePath">Existing Image</label><br><br>
                      
                      <img src="<?echo $includes_dir."Exhibitions/".$AB->Path?>" alt="">
                    </div>
                    
                  </div><!-- /.box-body -->
                  <? } ?>
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save Changes</button>
                  </div>
                </form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 2.3.0
        </div>
        <strong>Copyright &copy; 2014-2015 <a href="http://ahsangondal.com">Ahsan Gondal</a>.</strong> All rights reserved.
      </footer>

      <!-- Control Sidebar -->
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="<?echo $includes_dir;?>AdminLTE/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?echo $includes_dir;?>AdminLTE/bootstrap/js/bootstrap.min.js"></script>

    <!-- DataTables -->
     
<script type="text/javascript" src="https://cdn.datatables.net/r/dt/dt-1.10.9,fh-3.0.0,kt-2.0.0,r-1.0.7,se-1.0.1/datatables.min.js"></script>
    <!-- SlimScroll -->
    <script src="<?echo $includes_dir;?>AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?echo $includes_dir;?>AdminLTE/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?echo $includes_dir;?>AdminLTE/dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?echo $includes_dir;?>AdminLTE/dist/js/demo.js"></script>


    <script src="https://cdn.ckeditor.com/4.4.3/standard/ckeditor.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.2.8/jquery.form-validator.min.js"></script>
   <script>
      $(function () {
        
        $.validate();
      });
    </script>


    <script>
      $(function () {
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        if(CKEDITOR){
        CKEDITOR.replace('Description');
        }
        //bootstrap WYSIHTML5 - text editor
        
      });
    </script>
    <!-- page script -->
    <script>
      $(function () {
        $("#example1").dataTable({
          responsive: true

        });
       
      });
    </script>
  </body>
</html>
