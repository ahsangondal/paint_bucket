<!DOCTYPE HTML>
<html lang="en-US">
  <head>
    <title>Press | The PaintBucket</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="<?php echo $includes_dir?>common-files/images/favicon.png" type="image/png">
    <link rel="stylesheet" href="<?php echo $includes_dir?>common-files/css/style.min.css"/>
    <link rel="stylesheet" id="scheme-source" href="<?php echo $includes_dir?>common-files/css/schemes/gray.css" />
    <link rel="stylesheet" id="scheme-source" href="<?php echo $includes_dir?>common-files/css/custom.css" />
    <!-- BEGIN PAGE STYLE -->
    <link rel="stylesheet" href="<?php echo $includes_dir?>common-files/plugins/magnific/magnific-popup.min.css" />
    
    <!-- END PAGE STYLE -->
    <!-- [if lt IE 9]>
      <script src="http://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="http://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif] -->
  </head>
  <body>
    <!-- BEGIN PRELOADER -->
    <div class="loader-overlay">
      <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
      </div>
    </div>
    <!-- END PRELOADER -->
<? //var_dump($artists) ?>
    <!--  Begin Topbar simple -->
    <div class="topnav fixed-topnav transparent full-">
      
      <div class="section">
        <div id="topbar-hold" class="nav-hold " style="margin-left:3%;margin-right:3%;">
          <nav class="navbar" role="navigation">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              </button>
              <!-- Site Name -->
              <a class="site-name navbar-brand" href="../homepage/index-slider-parallax.html"></a>
            </div>
            <!-- Main Navigation menu Starts -->
            <div class="navbar-collapse collapse">
              <ul class="nav navbar-nav navbar-right">
                			<? $this->load->view('menu');?>
              </ul>
            </div>
            <!-- Main Navigation menu ends-->
          </nav>
        </div>
      </div>
    </div>
    <!--  End Topbar simple -->

    <!-- Header Parallax Image -->
    <header class="center header-md bg-dark parallax" data-img="bg/smoke2.png" data-speed="0.5">
      <div class="container">
        <div class="top-text f-left fade-text">
          <h1 class="header-title " >Press</h1>
        </div>
        <div class="top-text ">
          <br>
          
        </div>
        <div class="breadcrumb">
          <a href="<?echo $base_url?>">Home</a>
          <a href="<?echo $base_url?>press">Press</a>
          
        </div>
      </div>
    </header>
    <!-- End Header Parallax Image -->
    <div class="full-width">
    <section class="section-contact  ">
      <div class=" animated" data-animation="fadeIn" data-animation-delay="500">
        <div class="row">
              
            <div class="col-md-6 ">
                 <img src="<? echo $includes_dir?>Press/1.png" style="width:100%"></img>
                
              
            </div>
            <div class="col-md-6 ">
                  <h3>Press Coverage Links</h3>
              
                  
                  <ul class="fa-ul">
                    <? foreach ($press as $key ) {
                      # code...
                    ?>
                    <li><a href="<? echo $key->link;?>"><i class="fa-li fa fa-spinner fa-spin"></i><? echo $key->link;?></a></li>
                    <?
                    }
                    ?>
                    <!-- <li><a href="http://www.pakistantoday.com.pk/2014/02/22/entertainment/the-paint-bucket-gallery-to-find-the-properties-of-imagination/"><i class="fa-li fa fa-spinner fa-spin"></i>http://www.pakistantoday.com.pk/2014/02/22/entertainment/the-paint-bucket-gallery-to-find-the-properties-of-imagination/</a></li>

                    <li><a href="http://www.siddysays.com/the-paint-bucket-gallery/"><i class="fa-li fa fa-spinner fa-spin"></i>http://www.siddysays.com/the-paint-bucket-gallery/</a></li>

                    <li><a href="http://www.fashioncentral.pk/people-parties/events/exhibitions/story-1118-paint-buckets-launch-exhibition/"><i class="fa-li fa fa-spinner fa-spin"></i>http://www.fashioncentral.pk/people-parties/events/exhibitions/story-1118-paint-buckets-launch-exhibition/</a></li>
                    <li><a href="http://tribune.com.pk/story/411725/the-paint-bucket-sells-art-online/"><i class="fa-li fa fa-spinner fa-spin"></i>http://tribune.com.pk/story/411725/the-paint-bucket-sells-art-online/</a></li>

                    <li><a href="http://www.fashiontrends.pk/pakistani/paint-buckets-launch-exhibition/"><i class="fa-li fa fa-spinner fa-spin"></i>http://www.fashiontrends.pk/pakistani/paint-buckets-launch-exhibition/</a></li>
                    <li><a href="http://sunday.com.pk/tag/The%20Paint%20Bucket%20Gallery"><i class="fa-li fa fa-spinner fa-spin"></i>http://sunday.com.pk/tag/The%20Paint%20Bucket%20Gallery</li>

                    <li><a href="http://tribune.com.pk/story/675214/finding-the-properties-of-imagination/"><i class="fa-li fa fa-spinner fa-spin"></i>http://tribune.com.pk/story/675214/finding-the-properties-of-imagination/</a></li>
                    <li><a href="http://www.siddysays.com/resolving-rights/"><i class="fa-li fa fa-spinner fa-spin"></i>http://www.siddysays.com/resolving-rights/</a></li> -->

                  </ul>
              
            </div>
            
          
        </div>
      </div>      
    </section>
    </div>
    <!-- End Call to Action Contact -->

    <!-- Begin Footer 3 columns Dark -->
    <div class="section-footer footer-wrap bg-dark">
      <div class="container footer center">
        <!-- <div class="row">
          <div class="col-md-3">
            <h4>Company Info</h4>
            <p>Web agency created in 2014</p>
            <p>Specialized in theme design template</p>
            <p>Contact us for custom project</p>
            <p>Make your own template with us</p>
          </div>
          <div class="col-md-3">
            <h4>Contact Info</h4>
            <p><i class="line-icon-map"></i>44 Main Street, New York, NY 25442</p>
            <p><i class="line-icon-screen-smartphone"></i>+2-777-555-332 / -2-666-442-887</p>
            <p><i class="line-icon-envelope-open"></i>support@themes-lab.com</p>
            <p><i class="line-icon-calendar"></i>9am - 6 pm monday / saturday</p>
          </div>
          <div class="col-md-3">
            <h4>Support</h4>
            <p><a href="#">Faq</a></p>
            <p><a href="#">Terms of Services</a></p>
            <p><a href="#">Privacy Policy</a></p>
            <p><a href="#">Contact us</a></p>
          </div>
          <div class="col-md-3">
            <h4>Get In Touch</h4>
            <div class="social-icons social-square">
              <ul class="text-left">
                <li><a href="#" class="animated" data-animation="fadeIn" data-animation-delay="300"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#" class="animated" data-animation="fadeIn" data-animation-delay="500"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#" class="animated" data-animation="fadeIn" data-animation-delay="700"><i class="fa fa-google-plus"></i></a></li>
                <li><a href="#" class="animated" data-animation="fadeIn" data-animation-delay="900"><i class="fa fa-pinterest"></i></a></li>
                <li><a href="#" class="animated" data-animation="fadeIn" data-animation-delay="1000"><i class="fa fa-flickr"></i></a></li>
                <li><a href="#" class="animated" data-animation="fadeIn" data-animation-delay="1200"><i class="fa fa-linkedin"></i></a></li>
              </ul>
            </div>
          </div>
        </div> -->
      </div>
    </div>
    <!-- End Footer 3 columns Dark -->

    <!-- Begin Copyright Dark -->
    <div class="section-copyright bg-dark">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <p>Ahsan Gondal, All rights reserved &copy; 2015</p>
          </div>
          <div class="col-md-6">
            <p class="copyright-simple-menu">
              <span>Home</span>
              <span>Collection</span>
              <span>Artists</span>
              <span>Exhibitions</span>
              <span>Press</span>
              <span>Contact</span>
            </p>
          </div>
        </div>
      </div>
    </div>
    <!-- End Copyright Dark -->

    <a href="#" class="scrollup"><i class="fa fa-angle-up"></i></a>

    <script src="<?php echo $includes_dir?>common-files/plugins/jquery/jquery-1.11.1.min.js"></script>
    <script src="<?php echo $includes_dir?>common-files/plugins/bootstrap/bootstrap.min.js"></script>
    <script src="<?php echo $includes_dir?>common-files/plugins/modernizr/modernizr.js"></script>
    <script src="<?php echo $includes_dir?>common-files/plugins/retina/retina.min.js"></script>
    <script src="<?php echo $includes_dir?>common-files/plugins/jquery/jquery.easing.1.3.js"></script>
    <script src="<?php echo $includes_dir?>common-files/plugins/appear/jquery.appear.min.js"></script>
    <script src="<?php echo $includes_dir?>common-files/plugins/smoothscroll/smoothscroll.min.js"></script>
    <script src="<?php echo $includes_dir?>common-files/plugins/smart-menu/jquery.smartmenus.min.js"></script>
    <!-- BEGIN PAGE SCRIPTS -->
    <script src="<?php echo $includes_dir?>common-files/plugins/parallax/scripts/jquery.parallax-1.1.3.js"></script>
    <script src="<?php echo $includes_dir?>common-files/plugins/magnific/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo $includes_dir?>common-files/plugins/isotope/isotope.pkgd.min.js"></script>
    <!-- END PAGE SCRIPTS -->
    <script src="<?php echo $includes_dir?>common-files/js/application.js"></script>
    <script type="text/javascript">
    $( document ).ready(function() {
      $('.html-popup').magnificPopup({type: 'ajax'});
    console.log( "ready!" );

			$("#press").addClass("current");
      // $( ".port-container" ).each(function(index) {

        
      //   if(($(this).find(".img-responsive").height()-$(this).height())/2<0){
      //   $(this).find(".img-responsive").css("margin-top", -($(this).find(".img-responsive").height()-$(this).height())/2);
      //   }
        

      // });
    });

		</script>
  </body>
</html>