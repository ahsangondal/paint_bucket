<!DOCTYPE html>
<html lang="en">
	<head>
	<!-- Meta -->
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
		<meta name = "description" content = "The Paint Bucket is fundamentally an Online Gallery, and the first of its kind in Pakistan. The gallery's primary aim is 

to promote fresh upcoming young artists and the exciting new work that they are creating. The Gallery aspires to 

provide emerging artists with a platform for their ambitious projects and to give them an opportunity to be recognized 

in the international art community. The Paint bucket also believes in making art affordable accessible to all." />
		
	<!-- Favicon -->
		<link rel="shortcut icon" href="images/favicon.ico" />
		
	<!-- Styles -->
		<link href="<?php echo $includes_dir?>styles/main.css" rel="stylesheet" />
		
	<!-- Plugins -->
		<link href="<?php echo $includes_dir?>plugins/flexslider/flexslider.css" rel="stylesheet" />
		<link href="<?php echo $includes_dir?>plugins/flexslider/skin.css" rel="stylesheet" />
		<link href="<?php echo $includes_dir?>plugins/colorbox/colorbox.css" rel="stylesheet" />
		<link href="<?php echo $includes_dir?>plugins/colorbox/skin.css" rel="stylesheet" />
		<link href="<?php echo $includes_dir?>plugins/mediaelement/mediaelementplayer.min.css" rel="stylesheet" />
		<link href="<?php echo $includes_dir?>plugins/mediaelement/skin.css" rel="stylesheet" />
		
	<!-- IE -->
		<!--[if lt IE 9]>
			<script src="scripts/ie/html5.js"></script>
			<link href="styles/ie/ie.css" rel="stylesheet" />
		<![endif]-->
		
	<!-- Title -->
		<title>The PaintBucket Gallery</title>
	</head>
	
	<body>
	<!-- MAIN WRAP -->
		<div id="main-wrap">
				
			<div id="page" class="fullscreen no-scroll">
				<ul id="home-accordion">
					<li>
						<a href="<? echo $base_url; ?>about">
							<img data-pos="0.5" src="<?php echo $includes_dir?>img/img1a.jpg" alt="Image" />
						</a>
						<div style="">
						<!-- <h1 style="position:absolute;top:50%;z-index:100;background-color:rgba(0,0,0,0.5);color:white">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;HOME&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h1> -->
						</div>
						<div class="fullscreen-caption" >

							<!-- <h1 style="color:rgba(0,0,0,0);visible:hidden;">.</h1> -->
							<!-- <h4> -->
								<!-- <span class="slide">We make beautiful </span> -->
								<!-- <span class="slide">applications for the web</span> -->
							<!-- </h4> -->
							<!-- <div style="padding-bottom: 60%;"></div> -->
							<!-- <h4>
								<span class="slide">
								Lorem ipsum dolor sit amet
								</span>
							</h4>
							<button class="color">
								<i class="small briefcase icon l"></i>
								<span class="inline-block">View our work</span>
							</button> -->
						</div>
					</li>
					
					<li>
						<a href="<?echo $base_url;?>collection">
							<img data-pos="0.52" src="<?php echo $includes_dir?>img/img3a.jpg" alt="Image" />
						</a>
						<div class="fullscreen-caption">
							<!-- <h2>Valid HTML5 Code</h2>
							<h4>
								<span class="slide">Suspendisse non </span>
								<span class="slide">lectus et justo placerat rhoncus</span>
							</h4>
							<button class="color">
								<i class="small check icon l"></i>
								<span class="inline-block">Validate now</span>
							</button> -->
						</div>
					</li>
					
					<li>
						<a href="<? echo $base_url; ?>artists">
							<img data-pos="0.5" src="<?php echo $includes_dir?>img/img6a.jpg" alt="Image" />
						</a>
						<div class="fullscreen-caption">
							<!-- <h2>Responsive Design</h2>
							<h4>
								<span class="slide">Suspendisse non </span>
								<span class="slide">lectus et justo placerat rhoncus</span>
							</h4> -->
						</div>
					</li>
					
					<li>
						<a href="<? echo $base_url; ?>exhibitions">
							<img data-pos="0.5" src="<?php echo $includes_dir?>img/img4a.jpg" alt="Image" />
						</a>
						<div class="fullscreen-caption">
							<!-- <h2>Awesome <br/>Portfolio Layouts</h2>
							<h4>
								<span class="slide">Suspendisse non </span>
								<span class="slide">lectus et justo placerat rhoncus</span>
							</h4>
							<div>
								<button class="small slide">
									<i class="small fire icon l"></i>
									<span class="inline-block">Do Something</span>
								</button>
								
								<button class="small slide">
									<i class="small drop icon l"></i>
									<span class="inline-block">Or Something Else</span>
								</button>
							</div> -->
						</div>
					</li>
					
					<li>
						<a href="<? echo $base_url; ?>press">
							<img data-pos="0.5" src="<?php echo $includes_dir?>img/img2a.jpg" alt="Image" />
						</a>
						<div class="fullscreen-caption">
							<!-- <h2>4 Home Page Layouts</h2>
							<h4>
								<span class="slide">Suspendisse non </span>
								<span class="slide">lectus et justo placerat rhoncus</span>
							</h4> -->
						</div>
					</li>
				</ul>
				
			</div><!-- #page -->
			
			<div id="background">
			</div><!-- #background -->
			
			<div id="dock">
				 <header id="header">
					<div id="site-logo">
						<a href="index.html">
							<img src="<?php echo $includes_dir?>img/logo.png" alt="Site Logo" />
						</a>
					</div><!-- #site-logo -->
					
					<nav id="main-nav">
						<ul>
                			<? $this->load->view('menu');?>
							
						</ul>

					</nav><!-- #main-nav -->
				 </header><!-- #header -->
				 
				 <footer id="footer">
					<div id="copyright" >
						<p >&copy; <a href="http://ahsangondal.com" class="normal">Ahsan Gondal </a> All Rights Reserved</p>
						<!-- <p>True Story is a responsive HTML5 template by <a href="#">Tee Fouad</a></p> -->
					</div><!-- #copyright -->
					
					<div id="social-media">

						<p>Stay Connected</p>
						<a href="https://twitter.com/BucketPaint" class="icon twitter">Twitter</a>
						<a href="https://www.facebook.com/ThePaintBucket" class="icon facebook">Facebook</a>
						<a href="https://pk.linkedin.com/pub/the-paint-bucket-gallery/90/821/b0b" class="icon linkedin">LinkedIn</a>
					</div><!-- #social-media -->
				 </footer><!-- #footer -->
				 
				 <a href="#" id="toggle-dock"><span><span>
					<span class="active-text">Hide dock</span>
					<span class="inactive-text">Show dock</span>
				</span></span></a>
				
				 <a href="#" id="page-top-link"><span><span>Back to top</span></span></a>
			</div><!-- #dock -->
			
			<div id="preloader">
				<span><span>Loading</span></span>
			</div><!-- #preloader -->
			
		</div><!-- #main-wrap -->
		
	<!-- SCRIPTS -->
		<script src="<?php echo $includes_dir?>scripts/jquery.1.7.2.js"></script>
		<script src="<?php echo $includes_dir?>scripts/jquery.easing.js"></script>
		<script src="<?php echo $includes_dir?>scripts/jquery.cookie.js"></script>
		<script src="<?php echo $includes_dir?>scripts/main.js"></script>
		
	<!-- Plugins -->
		<script src="<?php echo $includes_dir?>plugins/modernizr/modernizr.min.js"></script>
		<script src="<?php echo $includes_dir?>plugins/flexslider/jquery.flexslider.min.js"></script>
		<script src="<?php echo $includes_dir?>plugins/colorbox/jquery.colorbox.min.js"></script>
		<script src="<?php echo $includes_dir?>plugins/mediaelement/mediaelement-and-player.min.js"></script>
		<script src="<?php echo $includes_dir?>plugins/social/jquery.social.js"></script>
		<script type="text/javascript">
			$("#home").addClass("current");

		</script>
	<!-- IE -->
		<!--[if lt IE 9]>
			<script src="<?php echo $includes_dir?>scripts/ie/ie.js" type="text/javascript"></script>
		<![endif]-->
	</body>
</html>