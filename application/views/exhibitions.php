<!DOCTYPE HTML>
<html lang="en-US">
  <head>
    <title>Exhibitions | The PaintBucket</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="<?php echo $includes_dir?>common-files/images/favicon.png" type="image/png">
    <link rel="stylesheet" href="<?php echo $includes_dir?>common-files/css/style.min.css"/>
    <link rel="stylesheet" id="scheme-source" href="<?php echo $includes_dir?>common-files/css/schemes/gray.css" />
    <link rel="stylesheet" id="scheme-source" href="<?php echo $includes_dir?>common-files/css/custom.css" />
    <!-- BEGIN PAGE STYLE -->
    <link rel="stylesheet" href="<?php echo $includes_dir?>common-files/plugins/magnific/magnific-popup.min.css" />
    <!-- END PAGE STYLE -->
    <!-- [if lt IE 9]>
      <script src="http://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="http://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif] -->
  </head>
  <body>
    <!-- BEGIN PRELOADER -->
    <div class="loader-overlay">
      <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
      </div>
    </div>
    <!-- END PRELOADER -->
<? //var_dump($artists) ?>
    <!--  Begin Topbar simple -->
    <div class="topnav fixed-topnav transparent full-">
      
      <div class="section">
        <div id="topbar-hold" class="nav-hold " style="margin-left:3%;margin-right:3%;">
          <nav class="navbar" role="navigation">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              </button>
              <!-- Site Name -->
              <a class="site-name navbar-brand" href="../homepage/index-slider-parallax.html"></a>
            </div>
            <!-- Main Navigation menu Starts -->
            <div class="navbar-collapse collapse">
              <ul class="nav navbar-nav navbar-right">
                			<? $this->load->view('menu');?>
              </ul>
            </div>
            <!-- Main Navigation menu ends-->
          </nav>
        </div>
      </div>
    </div>
    <!--  End Topbar simple -->

    <!-- Header Parallax Image -->
    <header class="center header-md bg-dark parallax" data-img="bg/smoke2.png" data-speed="0.5">
      <div class="container">
        <!-- <div class="top-text fade-text " style="text-align:center">
          <h1 class="header-title " style="text-align:center">Artists</h1>
        </div> -->
        <div class="top-text f-left fade-text">
          <h1 class="header-title text-left">Exhibitions</h1>
          <p class="header-message text-left">Take a sneak peek at our extraordinary people.</p>
        </div>
        <div class="breadcrumb">
          <a href="<?echo $base_url?>">Home</a>
          <a href="<?echo $base_url?>exhibitions">Exhibitions</a>
          
        </div>
      </div>
    </header>
    <!-- End Header Parallax Image -->

    <!-- Begin Portfolio Boxed 4 columns with Space -->
     <section class="section-portfolio bg-gray-light full-width">
      <div class="full-width" style="margin-left:3%;margin-right:3%;">
        <!--  -->
        <div class="portfolioContainer grid-3 with-text">
          <? foreach ($exhibitions as $a) {
          	# code...
          ?>
          <figure class="effect-jazz ">
            <figcaption>
              <div class="see-more more-btn">
                <div class="see-more-inner">
                  <a href="<?echo $base_url."exhibitions/details/".$a->Slug?>" class="btn button-line button-white btn-rounded hover-effect">View Details</a>
                </div>
              </div>
            </figcaption>
            <div class="portfolio-img thickbox">
              <img src="includes/Exhibitions/<?echo $a->ImagePath?>" alt="7">
            </div>  
            <div class="portofolio-desc">
              <h4><a href="<?echo $base_url."exhibitions/details/".$a->Slug?>"><?echo $a->ExhibitionName?></a></h4>
              
            </div>
          </figure>

          <?}?>

          
        </div>
      </div>    
    </section>
    <!-- End Portfolio Boxed 4 columns with Space -->

    <!-- Begin Call to Action Contact -->
    <section class="section-call-action bg-gray-light">
      <div class="container center">
        <div class="row">
          <div class="col-md-12">
            <h3 class="section-title">Have artwork? We're here to help you share it with the world.</h3>
          </div>
          <div class="col-md-12">
            <a href="<?echo base_url?>contact" class="btn btn-dark btn-rounded hover-effect">Contact Us</a>
          </div>
        </div>
      </div>
    </section>
    <!-- End Call to Action Contact -->

    <!-- Begin Footer 3 columns Dark -->
    <div class="section-footer footer-wrap bg-dark">
      <div class="container footer center">
        <!-- <div class="row">
          <div class="col-md-3">
            <h4>Company Info</h4>
            <p>Web agency created in 2014</p>
            <p>Specialized in theme design template</p>
            <p>Contact us for custom project</p>
            <p>Make your own template with us</p>
          </div>
          <div class="col-md-3">
            <h4>Contact Info</h4>
            <p><i class="line-icon-map"></i>44 Main Street, New York, NY 25442</p>
            <p><i class="line-icon-screen-smartphone"></i>+2-777-555-332 / -2-666-442-887</p>
            <p><i class="line-icon-envelope-open"></i>support@themes-lab.com</p>
            <p><i class="line-icon-calendar"></i>9am - 6 pm monday / saturday</p>
          </div>
          <div class="col-md-3">
            <h4>Support</h4>
            <p><a href="#">Faq</a></p>
            <p><a href="#">Terms of Services</a></p>
            <p><a href="#">Privacy Policy</a></p>
            <p><a href="#">Contact us</a></p>
          </div>
          <div class="col-md-3">
            <h4>Get In Touch</h4>
            <div class="social-icons social-square">
              <ul class="text-left">
                <li><a href="#" class="animated" data-animation="fadeIn" data-animation-delay="300"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#" class="animated" data-animation="fadeIn" data-animation-delay="500"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#" class="animated" data-animation="fadeIn" data-animation-delay="700"><i class="fa fa-google-plus"></i></a></li>
                <li><a href="#" class="animated" data-animation="fadeIn" data-animation-delay="900"><i class="fa fa-pinterest"></i></a></li>
                <li><a href="#" class="animated" data-animation="fadeIn" data-animation-delay="1000"><i class="fa fa-flickr"></i></a></li>
                <li><a href="#" class="animated" data-animation="fadeIn" data-animation-delay="1200"><i class="fa fa-linkedin"></i></a></li>
              </ul>
            </div>
          </div>
        </div> -->
      </div>
    </div>
    <!-- End Footer 3 columns Dark -->

    <!-- Begin Copyright Dark -->
    <div class="section-copyright bg-dark">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <p>Ahsan Gondal, All rights reserved &copy; 2015</p>
          </div>
          <div class="col-md-6">
            <p class="copyright-simple-menu">
              <span>Home</span>
              <span>Collection</span>
              <span>Artists</span>
              <span>Exhibitions</span>
              <span>Press</span>
              <span>Contact</span>
            </p>
          </div>
        </div>
      </div>
    </div>
    <!-- End Copyright Dark -->

    <a href="#" class="scrollup"><i class="fa fa-angle-up"></i></a>

    <script src="<?php echo $includes_dir?>common-files/plugins/jquery/jquery-1.11.1.min.js"></script>
    <script src="<?php echo $includes_dir?>common-files/plugins/bootstrap/bootstrap.min.js"></script>
    <script src="<?php echo $includes_dir?>common-files/plugins/modernizr/modernizr.js"></script>
    <script src="<?php echo $includes_dir?>common-files/plugins/retina/retina.min.js"></script>
    <script src="<?php echo $includes_dir?>common-files/plugins/jquery/jquery.easing.1.3.js"></script>
    <script src="<?php echo $includes_dir?>common-files/plugins/appear/jquery.appear.min.js"></script>
    <script src="<?php echo $includes_dir?>common-files/plugins/smoothscroll/smoothscroll.min.js"></script>
    <script src="<?php echo $includes_dir?>common-files/plugins/smart-menu/jquery.smartmenus.min.js"></script>
    <!-- BEGIN PAGE SCRIPTS -->
    <script src="<?php echo $includes_dir?>common-files/plugins/parallax/scripts/jquery.parallax-1.1.3.js"></script>
    <script src="<?php echo $includes_dir?>common-files/plugins/magnific/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo $includes_dir?>common-files/plugins/isotope/isotope.pkgd.min.js"></script>
    <!-- END PAGE SCRIPTS -->
    <script src="<?php echo $includes_dir?>common-files/js/application.js"></script>
    <script type="text/javascript">
			$("#exhibtions").addClass("current");

		</script>
  </body>
</html>