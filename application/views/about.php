<!DOCTYPE HTML>
<html lang="en-US">
  <head>
    <title>Press | The PaintBucket</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="<?php echo $includes_dir?>common-files/images/favicon.png" type="image/png">
    <link rel="stylesheet" href="<?php echo $includes_dir?>common-files/css/style.min.css"/>
    <link rel="stylesheet" id="scheme-source" href="<?php echo $includes_dir?>common-files/css/schemes/gray.css" />
    <link rel="stylesheet" id="scheme-source" href="<?php echo $includes_dir?>common-files/css/custom.css" />
    <!-- BEGIN PAGE STYLE -->
    <link rel="stylesheet" href="<?php echo $includes_dir?>common-files/plugins/magnific/magnific-popup.min.css" />
    
    <!-- END PAGE STYLE -->
    <!-- [if lt IE 9]>
      <script src="http://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="http://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif] -->
  </head>
  <body>
    <!-- BEGIN PRELOADER -->
    <div class="loader-overlay">
      <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
      </div>
    </div>
    <!-- END PRELOADER -->
<? //var_dump($artists) ?>
    <!--  Begin Topbar simple -->
    <div class="topnav fixed-topnav transparent full-">
      
      <div class="section">
        <div id="topbar-hold" class="nav-hold " style="margin-left:3%;margin-right:3%;">
          <nav class="navbar" role="navigation">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              </button>
              <!-- Site Name -->
              <a class="site-name navbar-brand" href="../homepage/index-slider-parallax.html"></a>
            </div>
            <!-- Main Navigation menu Starts -->
            <div class="navbar-collapse collapse">
              <ul class="nav navbar-nav navbar-right">
                			<? $this->load->view('menu');?>
              </ul>
            </div>
            <!-- Main Navigation menu ends-->
          </nav>
        </div>
      </div>
    </div>
    <!--  End Topbar simple -->

    <!-- Header Parallax Image -->
    <header class="center header-md bg-dark parallax" data-img="bg/smoke2.png" data-speed="0.5">
      <div class="container">
        <div class="top-text f-left fade-text">
          <h1 class="header-title " >About</h1>
        </div>
        <div class="top-text ">
          <br>
          
        </div>
        <div class="breadcrumb">
          <a href="<?echo $base_url?>">Home</a>
          <a href="<?echo $base_url?>press">About</a>
          
        </div>
      </div>
    </header>
    <!-- End Header Parallax Image -->
    <div class="full-width">
    <section class="section-contact  ">
      <div class=" animated" data-animation="fadeIn" data-animation-delay="500">
        <div class="row">
              
            <div class="col-md-6 ">
                 <img src="<? echo $includes_dir?>common-files/about.jpg" style="width:100%"></img>
                
              
            </div>
            <div class="col-md-6 " >
              <h2>A LITTLE ABOUT US</h1>
                  <p class="justify">The Paint Bucket is fundamentally an Online Gallery, and the first of its kind in Pakistan. The gallery's primary aim is 

to promote fresh upcoming young artists and the exciting new work that they are creating. The Gallery aspires to 

provide emerging artists with a platform for their ambitious projects and to give them an opportunity to be recognized 

in the international art community. The Paint bucket also believes in making art affordable accessible to all.</p>
                  <p class="justify">The Pakistani art scene is growing rapidly, and now its presence in the international arena is turning heads. More 

and more galleries, collectors and platforms are keen to acquire contemporary art from Pakistan. keeping this in mind 

and seeing as there is hardly any opportunity for many young Pakistani artists to promote themselves on a virtual 

platform that is accessible to local as well as international audiences, The Paint Bucket Gallery feels that they can 

help by bridging this gap in a big way.</p>
<p class="justify">Pakistan is still new to virtual galleries, whereas the world has been holding online exhibitions, auctions and art fairs 

for quite a few years now and has been successfully promoting art globally. The Paint Bucket hopes to follow in this 

direction.</p>                  
<p class="justify">The Paint bucket also believes in making art affordable accessible to all.</p>                  
                  
              
            </div>
            
          
        </div>
      </div>      
    </section>
    </div>
    <!-- End Call to Action Contact -->

    <!-- Begin Footer 3 columns Dark -->
    <div class="section-footer footer-wrap bg-dark">
      <div class="container footer center">
        <!-- <div class="row">
          <div class="col-md-3">
            <h4>Company Info</h4>
            <p>Web agency created in 2014</p>
            <p>Specialized in theme design template</p>
            <p>Contact us for custom project</p>
            <p>Make your own template with us</p>
          </div>
          <div class="col-md-3">
            <h4>Contact Info</h4>
            <p><i class="line-icon-map"></i>44 Main Street, New York, NY 25442</p>
            <p><i class="line-icon-screen-smartphone"></i>+2-777-555-332 / -2-666-442-887</p>
            <p><i class="line-icon-envelope-open"></i>support@themes-lab.com</p>
            <p><i class="line-icon-calendar"></i>9am - 6 pm monday / saturday</p>
          </div>
          <div class="col-md-3">
            <h4>Support</h4>
            <p><a href="#">Faq</a></p>
            <p><a href="#">Terms of Services</a></p>
            <p><a href="#">Privacy Policy</a></p>
            <p><a href="#">Contact us</a></p>
          </div>
          <div class="col-md-3">
            <h4>Get In Touch</h4>
            <div class="social-icons social-square">
              <ul class="text-left">
                <li><a href="#" class="animated" data-animation="fadeIn" data-animation-delay="300"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#" class="animated" data-animation="fadeIn" data-animation-delay="500"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#" class="animated" data-animation="fadeIn" data-animation-delay="700"><i class="fa fa-google-plus"></i></a></li>
                <li><a href="#" class="animated" data-animation="fadeIn" data-animation-delay="900"><i class="fa fa-pinterest"></i></a></li>
                <li><a href="#" class="animated" data-animation="fadeIn" data-animation-delay="1000"><i class="fa fa-flickr"></i></a></li>
                <li><a href="#" class="animated" data-animation="fadeIn" data-animation-delay="1200"><i class="fa fa-linkedin"></i></a></li>
              </ul>
            </div>
          </div>
        </div> -->
      </div>
    </div>
    <!-- End Footer 3 columns Dark -->

    <!-- Begin Copyright Dark -->
    <div class="section-copyright bg-dark">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <p>Ahsan Gondal, All rights reserved &copy; 2015</p>
          </div>
          <div class="col-md-6">
            <p class="copyright-simple-menu">
              <span>Home</span>
              <span>Collection</span>
              <span>Artists</span>
              <span>Exhibitions</span>
              <span>Press</span>
              <span>Contact</span>
            </p>
          </div>
        </div>
      </div>
    </div>
    <!-- End Copyright Dark -->

    <a href="#" class="scrollup"><i class="fa fa-angle-up"></i></a>

    <script src="<?php echo $includes_dir?>common-files/plugins/jquery/jquery-1.11.1.min.js"></script>
    <script src="<?php echo $includes_dir?>common-files/plugins/bootstrap/bootstrap.min.js"></script>
    <script src="<?php echo $includes_dir?>common-files/plugins/modernizr/modernizr.js"></script>
    <script src="<?php echo $includes_dir?>common-files/plugins/retina/retina.min.js"></script>
    <script src="<?php echo $includes_dir?>common-files/plugins/jquery/jquery.easing.1.3.js"></script>
    <script src="<?php echo $includes_dir?>common-files/plugins/appear/jquery.appear.min.js"></script>
    <script src="<?php echo $includes_dir?>common-files/plugins/smoothscroll/smoothscroll.min.js"></script>
    <script src="<?php echo $includes_dir?>common-files/plugins/smart-menu/jquery.smartmenus.min.js"></script>
    <!-- BEGIN PAGE SCRIPTS -->
    <script src="<?php echo $includes_dir?>common-files/plugins/parallax/scripts/jquery.parallax-1.1.3.js"></script>
    <script src="<?php echo $includes_dir?>common-files/plugins/magnific/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo $includes_dir?>common-files/plugins/isotope/isotope.pkgd.min.js"></script>
    <!-- END PAGE SCRIPTS -->
    <script src="<?php echo $includes_dir?>common-files/js/application.js"></script>
    <script type="text/javascript">
    $( document ).ready(function() {
      $('.html-popup').magnificPopup({type: 'ajax'});
    console.log( "ready!" );

			$("#about").addClass("current");
      // $( ".port-container" ).each(function(index) {

        
      //   if(($(this).find(".img-responsive").height()-$(this).height())/2<0){
      //   $(this).find(".img-responsive").css("margin-top", -($(this).find(".img-responsive").height()-$(this).height())/2);
      //   }
        

      // });
    });

		</script>
  </body>
</html>