<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class paint_model extends CI_Model {
	
	// The following method prevents an error occurring when $this->data is modified.
	// Error Message: 'Indirect modification of overloaded property Demo_cart_admin_model::$data has no effect'.
	function __construct() {
		parent::__construct ();
		
		// To load the CI benchmark and memory usage profiler - set 1==1.
		if (1 == 2) {
			$sections = array (
					'benchmarks' => TRUE,
					'memory_usage' => TRUE,
					'config' => FALSE,
					'controller_info' => FALSE,
					'get' => FALSE,
					'post' => FALSE,
					'queries' => FALSE,
					'uri_string' => FALSE,
					'http_headers' => FALSE,
					'session_data' => FALSE 
			);
			$this->output->set_profiler_sections ( $sections );
			$this->output->enable_profiler ( TRUE );
		}
		
		// Load required CI libraries and helpers.
		$this->load->database ();
		$this->load->library ( 'session' );
		$this->load->helper ( 'url' );
		$this->load->helper ( 'form' );
		
		// IMPORTANT! This global must be defined BEFORE the flexi auth library is loaded!
		// It is used as a global that is accessible via both models and both libraries, without it, flexi auth will not work.
		
		// $this->auth = new stdClass ();
		// $this->auth->id = '';
		
		// Load 'standard' flexi auth library by default.
		// $this->load->library ( 'flexi_auth' );
		
		// Note: This is only included to create base urls for purposes of this demo only and are not necessarily considered as 'Best practice'.
		
		
		// Define a global variable to store data that is then used by the end view page.
		$this->data = null;
	}
	function getPress() {
		
			// $this->db->where ( 'company_id', $id );
			
			$this->db->select ( '*' );
			$this->db->from ( 'Press as a' );
			// $this->db->order_by('artistSlug','asc');
			$q=$this->db->get();
		if ($q->num_rows > 0) {
			return $q->result ();
		}
		return array ();
	}

	function getArtists() {
		
			// $this->db->where ( 'company_id', $id );
			
			$this->db->select ( '*' );
			$this->db->from ( 'artists as a' );
			$this->db->order_by('artistSlug','asc');
			$q=$this->db->get();
		if ($q->num_rows > 0) {
			return $q->result ();
		}
		return array ();
	}

	function getArtistsC($type) {
		
			// $this->db->where ( 'company_id', $id );
			
			$this->db->select ( '*' );
			$this->db->from ( 'artists as a' );
			$this->db->where ( 'type',$type );
			$this->db->order_by('artistSlug','asc');
			$q=$this->db->get();
		if ($q->num_rows > 0) {
			return $q->result ();
		}
		return array ();
	}

	function getExhibitions() {
		
			// $this->db->where ( 'company_id', $id );
			
			$this->db->select ( '*' );
			$this->db->from ( 'Exhibitions as a' );
			$this->db->order_by('Slug','asc');
			$q=$this->db->get();
		if ($q->num_rows > 0) {
			return $q->result ();
		}
		return array ();
	}
	function getArtistBio($id) {
		
			// $this->db->where ( 'company_id', $id );
			
			$this->db->select ( '*' );
			$this->db->from ( 'artists as a' );
			$this->db->where ( 'artistSlug',$id );
			$this->db->order_by('artistSlug','asc');

			$q=$this->db->get();
		if ($q->num_rows > 0) {
			return $q->result ();
		}
		return array ();
	}

	function getExhibitionInfo($id) {
		
			// $this->db->where ( 'company_id', $id );
			
			$this->db->select ( '*' );
			$this->db->from ( 'Exhibitions as a' );
			$this->db->where ( 'Slug',$id );
			$this->db->order_by('Slug','asc');

			$q=$this->db->get();
		if ($q->num_rows > 0) {
			return $q->result ();
		}
		return array ();
	}
	function getArtwork($id) {
		
			// $this->db->where ( 'company_id', $id );
			
			$this->db->select ( '*' );
			$this->db->from ( 'artwork as a' );
			$this->db->where ( 'artistSlug',$id );
			
			$this->db->order_by('artistSlug','asc');
			$q=$this->db->get();
		if ($q->num_rows > 0) {
			return $q->result ();
		}
		return array ();
	}

	function getExArtWork($id) {
		
			// $this->db->where ( 'company_id', $id );
			
			$this->db->select ( '*' );
			$this->db->from ( 'ExhibitionsPaintings as a' );
			$this->db->where ( 'Slug',$id );
			
			$this->db->order_by('Slug','asc');
			$q=$this->db->get();
		if ($q->num_rows > 0) {
			return $q->result ();
		}
		return array ();
	}
	function getPainting($id) {
		
			// $this->db->where ( 'company_id', $id );
			
			$this->db->select ( '*' );
			$this->db->from ( 'artwork as a' );
			$this->db->where ( 'contentId',$id );
			
			$this->db->order_by('contentId','asc');
			$q=$this->db->get();
		if ($q->num_rows > 0) {
			return $q->result ();
		}
		return array ();
	}
	function getExPainting($id) {
		
			// $this->db->where ( 'company_id', $id );
			
			$this->db->select ( '*' );
			$this->db->from ( 'ExhibitionsPaintings as a' );
			$this->db->where ( 'contentId',$id );
			
			$this->db->order_by('contentId','asc');
			$q=$this->db->get();
		if ($q->num_rows > 0) {
			return $q->result ();
		}
		return array ();
	}
	
	
	
}


					/* End of file demo_auth_admin_model.php */
/* Location: ./application/models/demo_auth_admin_model.php */